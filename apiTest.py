import requests

url = 'https://reqres.in/api/'


def test_registration():
    """Test1"""
    data = {
        "email": "eve.holt@reqres.in",
        "password": "pistol"
    }
    response = requests.post(url + "register", data=data)

    assert response.status_code == 200
    result = {'id': 4, 'token': 'QpwL5tke4Pnpja7X4'}
    assert result == response.json()


def test_login():
    """Test2"""
    data = {
        "email": "eve.holt@reqres.in",
        "password": "cityslicka"
    }
    response = requests.post(url + "login", data=data)
    assert response.status_code == 200
    result = {'token': 'QpwL5tke4Pnpja7X4'}
    assert result == response.json()


def test_list_resource():
    """Test3"""
    response = requests.get(url + 'unknown')
    assert response.status_code == 200
    data = response.json()['data'][4]
    response = requests.get(url + 'unknown/{}'.format(data['id']))
    assert response.status_code == 200
    assert data == response.json()['data']
