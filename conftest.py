import pytest
from selenium import webdriver
from helpers.locators import Locators
from helpers.functions import Functions


@pytest.fixture(scope="function")
def driver_init(request):

    driver = webdriver.Chrome()
    request.cls.url = 'https://www.yahoo.com/'
    request.cls.locator = Locators(driver)
    request.cls.function = Functions(driver)
    request.cls.driver = driver
