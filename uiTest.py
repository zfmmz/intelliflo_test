import time

import pytest
import unittest

from selenium.webdriver.common.by import By


@pytest.mark.usefixtures("driver_init")
class TestIntelliflo(unittest.TestCase):

    @classmethod
    def setUp(cls):
        cls.driver.get(cls.url)
        cls.driver.maximize_window()
        assert 'Yahoo is part of the Yahoo family of brands' in cls.function.get_page_title()
        cls.locator.click_element(By.ID, "scroll-down-btn")
        cls.locator.click_element(By.NAME, "reject", delay=20)
        assert cls.locator.wait_element(By.ID, "Main", delay=20)

    def tearDown(self):
        self.driver.quit()

    def test_successfully_login(self):
        """Test 4"""
        self.locator.click_element(By.XPATH, "//*[@id='ybarAccountProfile']//a[text()='Sign in']")
        assert self.locator.wait_element(By.ID, "login-body", delay=20)
        credentials = self.function.retrieve_user_credentials('iflotestuser1')
        self.locator.insert_text(credentials['username'], By.ID, "login-username")
        self.locator.click_element(By.ID, "login-signin")
        assert self.locator.wait_element(By.ID, "password-challenge", delay=20)
        self.locator.insert_text(credentials['password'], By.ID, "login-passwd")
        self.locator.click_element(By.ID, "login-signin")
        assert self.locator.wait_element(By.ID, "ybarAccountMenu", delay=20)

    def test_unsuccessful_login(self):
        """ Test 5 and 6"""
        invalid_users = ['iflotestuser2', 'iflotestuser1']
        for user in invalid_users:
            self.locator.click_element(By.XPATH, "//*[@id='ybarAccountProfile']//a[text()='Sign in']")
            assert self.locator.wait_element(By.ID, "login-body", delay=20)
            credentials = self.function.retrieve_user_credentials(user, 'invalid')
            self.locator.insert_text(credentials['username'], By.ID, "login-username")
            self.locator.click_element(By.ID, "login-signin")
            assert self.locator.wait_element(By.ID, "password-challenge", delay=20)
            self.locator.insert_text(credentials['password'], By.ID, "login-passwd")
            self.locator.click_element(By.ID, "login-signin")
            assert self.locator.wait_element(By.CLASS_NAME, "error-msg", delay=20)
            self.locator.click_element(By.CLASS_NAME, "logo")

    def test_verify_market_data(self):
        """Test 7"""
        self.locator.click_element(By.XPATH, "//*[@id='ybarAccountProfile']//a[text()='Sign in']")
        assert self.locator.wait_element(By.ID, "login-body", delay=20)
        credentials = self.function.retrieve_user_credentials('iflotestuser1')
        self.locator.insert_text(credentials['username'], By.ID, "login-username")
        self.locator.click_element(By.ID, "login-signin")
        assert self.locator.wait_element(By.ID, "password-challenge", delay=20)
        self.locator.insert_text(credentials['password'], By.ID, "login-passwd")
        self.locator.click_element(By.ID, "login-signin")
        self.locator.click_element(By.XPATH, "//a[contains(text(),'Finance')]")
        self.locator.hover_click_element('Calendar', By.XPATH, "//*[@title='Market Data']")
        assert self.locator.wait_element(By.XPATH, "//*[@data-test='ALL_EVENTS']")
        self.driver.execute_script("scrollBy(0,-500);")
        self.driver.find_element(By.XPATH, "//*[@data-test='dropdown']//*[@data-icon='caret-down']").click()
        assert self.locator.wait_element(By.XPATH, "//*[contains(@class,'datepicker')]", delay=5)
        self.locator.insert_text('05/09/23', By.XPATH, "//*[@id='dropdown-menu']//form/input", delay=5)
        self.locator.click_element(By.XPATH, "//*[@data-test='dt-picker-appl']")
        assert self.locator.wait_element(By.XPATH, "//*[@id='fin-cal-events']//ul")
        data = self.locator.select_elements(By.XPATH, "//*[@id='fin-cal-events']//ul/li[3]/a/span")
        assert len(data) == 4
        assert data == ['Earnings', 'Stock splits', 'IPO pricing', 'Economic events']


if __name__ == '__main__':
    pytest.main()

