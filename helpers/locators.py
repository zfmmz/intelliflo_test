from selenium.common.exceptions import TimeoutException, InvalidElementStateException, WebDriverException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from helpers.functions import Functions


class Locators(object):

    def __init__(self, driver):
        self.driver = driver
        self.function = Functions(self.driver)
        self.action = ActionChains(self.driver)

        try:
            self.driver.implicitly_wait(10)
        except WebDriverException as _:
            pass

    def insert_text(self, text, *loc, **kwargs):
        try:
            delay = kwargs.get('delay', 5)
            WebDriverWait(self.driver, delay).until(EC.presence_of_element_located(loc))
            element = self.driver.find_element(*loc)
            self.driver.execute_script("return arguments[0].scrollIntoView();", element)
            self.function.log('Clearing text field')
            self.driver.find_element(*loc).clear()

            element.send_keys(text)
            self.function.log('adding text to element: %s' % (loc[1]))
        except InvalidElementStateException as _:
            self.function.log(_)
            element.send_keys(text)

    def click_element(self, *loc, **kwargs):
        delay = kwargs.get('delay', 5)
        WebDriverWait(self.driver, delay).until(EC.presence_of_element_located(loc))
        element = self.driver.find_element(*loc)
        self.driver.execute_script("return arguments[0].scrollIntoView();", element)
        # element.click()
        self.driver.execute_script("arguments[0].click();", element)
        self.function.log('Click element: %s' % (loc[1]))

    def get_text(self, *loc, **kwargs):
        delay = kwargs.get('delay', 2)
        WebDriverWait(self.driver, delay).until(EC.presence_of_element_located(loc))
        element = self.driver.find_element(*loc)
        self.driver.execute_script("return arguments[0].scrollIntoView();", element)
        self.function.log('Getting text to element: %s' % (loc[1]))
        return element.text

    def select_element_drop_down(self, drop_down_text, *loc, **kwargs):
        delay = kwargs.get('delay', 5)
        WebDriverWait(self.driver, delay).until(EC.element_to_be_clickable(loc))
        element = Select(self.driver.find_element(*loc))
        element.select_by_visible_text(drop_down_text)

    def wait_element(self, *loc, **kwargs):
        delay = kwargs.get('delay', 10)
        try:
            WebDriverWait(self.driver, delay).until(EC.visibility_of_element_located(loc))
            element = self.driver.find_element(*loc)
            if element != '' or element is not None:
                self.driver.execute_script("return arguments[0].scrollIntoView();", element)
                self.function.log('Selecting element: %s' % (loc[1]))
                return True
            else:
                return False
        except TimeoutException as _:
            self.function.log('Element not found')
            self.function.log(_)
            return False

    def get_attribute_text(self, value, *loc, **kwargs):
        delay = kwargs.get('delay', 2)
        try:
            WebDriverWait(self.driver, delay).until(EC.presence_of_element_located(loc))
            element = self.driver.find_element(*loc)
            if element != '' or element is not None:
                self.driver.execute_script("return arguments[0].scrollIntoView();", element)
                self.function.log('Get element attribute: %s' % (loc[1]))
                return element.get_attribute(value)
            else:
                return False
        except TimeoutException as _:
            self.function.log('Element not found')
            self.function.log(_)
            return False

    def select_elements(self, *loc, **kwargs):
        delay = kwargs.get('delay', 5)
        try:
            WebDriverWait(self.driver, delay).until(EC.presence_of_element_located(loc))
            element = self.driver.find_elements(*loc)
            self.function.log('----')
            list_of_text_found = [str(found_text.text) for found_text in element]
            return list_of_text_found
        except TimeoutException as _:
            self.function.log('Element not found')
            self.function.log(_)

    def hover_click_element(self, clicked_menu, *loc, **kwargs):
        delay = kwargs.get('delay', 5)
        try:
            WebDriverWait(self.driver, delay).until(EC.presence_of_element_located(loc))
            menu_hovered = self.driver.find_element(*loc)
            ActionChains(self.driver).move_to_element(menu_hovered).perform()
            self.click_element(By.XPATH, "//*[contains(text(),'{}')]".format(clicked_menu), delay=5)
        except TimeoutException as _:
            self.function.log('Element not found')
            self.function.log(_)

