import logging
import os
import pandas as pd
from numbers_parser import Document


class Functions(object):

    def __init__(self, driver):
        self.driver = driver

    @staticmethod
    def log(text):
        # This method will be used to log to the console
        logging.getLogger().setLevel(logging.INFO)
        logging.info(text)

    def get_page_title(self):
        title = self.driver.title
        self.log('Title is: %s' % title)
        return title

    @staticmethod
    def retrieve_user_credentials(user, scenario='valid'):
        project_root = os.path.abspath(os.getcwd())
        data = pd.read_excel(os.path.join(project_root, "resources/credentials.xlsx"))

        for i, (username, password) in enumerate(zip(data['username'], data['password'])):
            if i == 0 and scenario == 'invalid':
                continue
            if username == user:
                credentials = dict()
                credentials['username'] = user
                credentials['password'] = password
                return credentials
